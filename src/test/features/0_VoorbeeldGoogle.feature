# language: nl
@all

Functionaliteit: Controleren of er gezocht kan worden in Google

  Scenario: Qquest opzoeken in Google en het dropdown resultaat controleren
    Als Ik ben op de Google homepage
    Wanneer Ik invul "Qquest"
    Dan Zie ik in een dropdown Qquest als resultaat

  Scenario: Qquest opzoeken in Google en de resultaatpagina controleren
    Als Ik ben op de Google homepage
    Wanneer Ik invul "Qquest"
    En Ik druk op Enter
    Dan Zie ik dat het eerste zoekresultaat Welkom bij Qquest is
