# language: nl
@all

Functionaliteit: Inloggen bij Ticket Tonite

  Abstract Scenario: Ik wil inloggen met een foute gebruikersnaam en/of wachtwoord
    Stel Ik ben
    En Ik klik
    En Ik zie
    Wanneer Ik klik op de knop Inloggen
    Dan Ik zie
    En Mijn inlognaam is <Gebruikersnaam>
    En Mijn
    Als Ik klik
    Dan Ik krijg

    Voorbeelden:
      | Gebruikersnaam |            |
      | "truus123"     | "groetjes" |

  Abstract Scenario: Ik wil inloggen
    Stel
    En
    En
    Wanneer
    Dan
    En Mijn inlognaam is <Gebruikersnaam>
    En
    Als
    Dan Ik ben ingelogd in Ticket Tonite

    Voorbeelden:
      | Gebruikersnaam   |  |
      | "Wappie123"      |  |
      | "Feestvarkentje" |  |

  Abstract Scenario: Ik wil op de confetti knop drukken om te vieren dat ik de allereerste Cucumber training ooit heb gehad
    Stel
    En
    En
    En
    En
    En
    En
    En
    Als
    Dan Ik druk op de groene knop Confetti en schreeuw keihard AAAAAAAAAAAA