# language: nl
@all

Functionaliteit: Registreren bij Ticket Tonite

  Abstract Scenario: Ik wil mezelf registreren
    Stel Ik ben op de team selectie pagina
    En Ik klik op het team Fantestic Automatics
    En Ik zie de beginpagina van de Ticket Tonite app
    En Ik klik op de knop Registreren
    Dan Ik zie dat ik mezelf gratis kan registreren
    Als Mijn e-mailadres is <E-mail>
    En Mijn gebruikersnaam is <Gebruikersnaam>
    En Mijn voornaam is <Voornaam>
    En Mijn achternaam is <Achternaam>
    En Mijn wachtwoord is <Wachtwoord>
    En Mijn eerste wachtwoord is ook <WachtwoordTwee>
    Wanneer Ik klik op Registreren
    Dan Mijn account is aangemaakt

    Voorbeelden:
      | E-mail             | Gebruikersnaam   | Voornaam | Achternaam | Wachtwoord      | WachtwoordTwee  |
      | "test@les.nl"      | "Wappie123"      | "Berend" | "Botje"    | "okdoei69"      | "okdoei69"      |
      | "feest@fabriek.nl" | "Feestvarkentje" | "Peppa"  | "Pig"      | "w8woordjuh234" | "w8woordjuh234" |