/*
 * Start file fur running test scenario's.
 * Click the right mouse button in this file and select `Test File` (NetBeans IDE).
 * Run the file with option dryRun = true to find missing step definitions.
 * Limit the scope of the test by changing the tags option, only scenario's with corresponding tags are executed.
 *
 */
package TestRunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/features/",
        glue = {"steps"},
        monochrome = true,
        tags = "@all",
        plugin = {"pretty",
                "html:testuitvoer/cucumber-reports",
                "json:testuitvoer/cucumber.json" }
//        dryRun = false
//        strict = true
)

public class RunAll {
    // stays empty
}

