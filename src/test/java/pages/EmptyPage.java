package pages;

import initialization.DriverUtils;
import initialization.EnvironmentData;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmptyPage {

    WebDriver driver;

    public EmptyPage(WebDriver driver) {
        // Deze constructor roept een lokale driver op, evenals de variabelen van de scherm elementen
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie website elementen
    //==============================================================================

    //** zoek op ID **//

    @FindBy(id = "L2AGLb")
    public WebElement popupButtonIkGaAkkoordByID;

    //** zoek met contains ID als het ID random delen heeft **//

    @FindBy(xpath = "//*[contains(@id, 'b1-Icon')]")
    public WebElement icoonGroenVinkje;

    //** zoek met contains TEXT **//

    @FindBy(xpath = "//span[contains(text(),'Qquest | IT-detachering & IT-Traineeship')]")
    private WebElement dropdownResultaatQquest;

    @FindBy(xpath = "//div[@class='QS5gu sy4vM'][contains(text(),'Aanpassen')]")
    private WebElement popupButtonAanpassen;

    @FindBy(xpath = "//div[@class='QS5gu sy4vM'][contains(text(),'Ik ga akkoord')]")
    private WebElement popupButtonIkGaAkkoordByText;

    //** zoek op TYPE **//

    @FindBy(xpath = "//input[@type='text']")
    public WebElement veldZoekterm;




    //==============================================================================
    // Definitie methodes die de interactie uitvoeren van de website elementen
    //==============================================================================

    public void voorbeeldOpenIntranetGildes() {
        // Navigeert in de browser naar de website
        String TargetURL = new EnvironmentData().GetEnvironmentURL(EnvironmentData.EnvironmentName.TST) + "gildes";
        driver.get(TargetURL);
    }
    public void klikVoorbeeld() {
        //** Wacht op het element **//
        new DriverUtils().WachtOpElement(driver,popupButtonAanpassen);
        //** Klikt op het element **//
        popupButtonAanpassen.click();
    }
    public void vulVoorbeeld(String zoekterm) {
        //** Wacht op het element **//
        new DriverUtils().WachtOpElement(driver,veldZoekterm);
        //** Vult de meegegeven string in het veld **//
        veldZoekterm.sendKeys(zoekterm);
    }
    public String dropdownResultaatQquest()  {
        //** Wacht op het element **//
        new DriverUtils().WachtOpElement(driver, dropdownResultaatQquest);
        //** Leest foutmelding af van het scherm **//
        return dropdownResultaatQquest.getText();
    }
    public void drukOpEnter() {
        //** Wacht op het element **//
        new DriverUtils().WachtOpElement(driver,veldZoekterm);
        //** Druk op een specifieke knop op het keyboard **//
        veldZoekterm.sendKeys(Keys.ENTER);
    }
    public void drukVeranderBestaandeTekst(String tekstArtikel) {
        //** Wacht op het element **//
        new DriverUtils().WachtOpElement(driver, veldZoekterm);
        //** Verwijder huidige tekst en vul de nieuwe in **//
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(tekstArtikel).build().perform();
    }

//    public void checkIcoonGroenVinkje(){
//        //** Wacht op het element **//
//        new DriverUtils().WachtOpElement(driver,icoonGroenVinkje);
//
//        // Controleert of er een groen vinkje verschijnt bij het invullen van een geldige postcode
//                 assertEquals("icon color-green fa fa-check fa-1x", icoonGroenVinkje.getClass());
//
//    }
//
//
//        icoonGroenVinkje.isDisplayed();





}
