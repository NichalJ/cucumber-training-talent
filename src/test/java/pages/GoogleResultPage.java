package pages;

import initialization.DriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleResultPage {

    WebDriver driver;

    public GoogleResultPage(WebDriver driver) {
        // Deze constructor roept een lokale driver op, evenals de variabelen van de scherm elementen
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie website elementen
    //==============================================================================

    @FindBy(xpath = "//h3[contains(text(),'Qquest: IT-specialisten Inhuren? | ? IT-traineeshi')]")
    private WebElement zoekResultaatQquest;

    //==============================================================================
    // Definitie methodes die de interactie uitvoeren van de website elementen
    //==============================================================================

    public void zoekResultaatQquest()  {
        // Leest foutmelding af van het scherm
        new DriverUtils().WachtOpElement(driver, zoekResultaatQquest);
    }



}
