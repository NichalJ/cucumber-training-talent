package pages;

import initialization.DriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InlogPage {

    WebDriver driver;

    public InlogPage(WebDriver driver) {
        // Deze constructor roept een lokale driver op, evenals de variabelen van de scherm elementen
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie website elementen
    //==============================================================================

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/input[1]")
    public WebElement veldInlognaam;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/input[1]")
    public WebElement veldInlogcode;

    @FindBy(xpath = "//h4[@id='mxui_widget_DialogMessage_0_caption']")
    public WebElement titelFoutmelding;

    @FindBy(xpath = "//button[@id='mxui_widget_LoginButton_0']")
    public WebElement knopAanmelden;

    @FindBy(xpath = "//h1[@class='mx-text mx-name-text40 pageheader-title spacing-outer-bottom']")
    public WebElement titelMijnEvents;

    //==============================================================================
    // Definitie methodes die de interactie uitvoeren van de website elementen
    //==============================================================================

    public void inputInlognaam(String inlognaam) {
        // Vult de inlognaam in
        new DriverUtils().WachtOpElement(driver, veldInlognaam);
        veldInlognaam.sendKeys(inlognaam);
    }

    public void inputInlogcode(String inlogcode) {
        // Vult de inlogcode in
        new DriverUtils().WachtOpElement(driver, veldInlogcode);
        veldInlogcode.sendKeys(inlogcode);
    }

    public void checkFoutmelding() {
        // Controleert aan de hand van de titel van de pop-up of je een foutmelding te zien krijgt
        new DriverUtils().WachtOpElement(driver, titelFoutmelding);
        titelFoutmelding.isDisplayed();
    }

    public void clickAanmelden() {
        // Klikt op de knop Aanmelden
        new DriverUtils().WachtOpElement(driver, knopAanmelden);
        knopAanmelden.click();
    }

    public void checkMijnEvents() {
        // Controleert aan de hand van de titel van de event pagina of je ingelogd bent
        new DriverUtils().WachtOpElement(driver, titelMijnEvents);
        titelMijnEvents.isDisplayed();
    }

}
