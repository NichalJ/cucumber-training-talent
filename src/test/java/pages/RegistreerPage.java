package pages;

import initialization.DriverUtils;
import initialization.EnvironmentData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistreerPage {

    WebDriver driver;

    public RegistreerPage(WebDriver driver) {
        // Deze constructor roept een lokale driver op, evenals de variabelen van de scherm elementen
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie website elementen
    //==============================================================================

    @FindBy(xpath = "//button[@class='btn mx-button mx-name-actionButton2 spacing-outer-top-large btn-primary']")
    public WebElement knopRegistreren;

    @FindBy(xpath = "//h1[@class='mx-text mx-name-text23 card-title spacing-outer-bottom-medium text-white spacing-outer-top-medium text-center d-block']")
    public WebElement checkTitel;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/input[1]")
    public WebElement veldEmail;

    @FindBy(xpath = "//button[@class='btn mx-button mx-name-actionButton3 btn-danger']")
    public WebElement knop2Registreren;

    @FindBy(xpath = "//h1[@class='mx-text mx-name-text23 card-title spacing-outer-bottom-medium text-white spacing-outer-top-medium text-center d-block']")
    public WebElement titelLogin;

    //==============================================================================
    // Definitie methodes die de interactie uitvoeren van de website elementen
    //==============================================================================


    public void inputEmail(String emailadres) {
        // Vult het e-mailadres in
        new DriverUtils().WachtOpElement(driver, veldEmail);
        veldEmail.sendKeys(emailadres);
    }

    public void clickRegistreren() {
        // Klikt op Registreren
        new DriverUtils().WachtOpElement(driver, knop2Registreren);
        knop2Registreren.click();
    }

    public void checkLogin() {
        // Controleert aan de hand van de titel of je na registratie op de inlogpagina bent beland
        new DriverUtils().WachtOpElement(driver, titelLogin);
        titelLogin.isDisplayed();
    }

}
