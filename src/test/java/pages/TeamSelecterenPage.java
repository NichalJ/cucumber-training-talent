package pages;

import initialization.DriverUtils;
import initialization.EnvironmentData;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TeamSelecterenPage {

    WebDriver driver;

    public TeamSelecterenPage(WebDriver driver) {
        // Deze constructor roept een lokale driver op, evenals de variabelen van de scherm elementen
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie website elementen
    //==============================================================================

    @FindBy(xpath = "//span[contains(text(),'Fantestic Automatics')]")
    public WebElement teamNaam;

    @FindBy(xpath = "//img[@class='mx-image mx-name-staticImage1 img-center img-rounded img-responsive']")
    public WebElement logoTT;

    //==============================================================================
    // Definitie methodes die de interactie uitvoeren van de website elementen
    //==============================================================================

    public void openTicketTonite() {
        // Navigeert in de browser naar TicketTonite
        String Environment = new EnvironmentData().GetEnvironmentURL(EnvironmentData.EnvironmentName.TICKETTONITE);
        driver.get(Environment);
    }

    public void clickTeamNaam() {
        // Klikt op de team naam
        new DriverUtils().WachtOpElement(driver, teamNaam);
        teamNaam.click();
    }

    public void checkLogoTT() {
        // Controleert aan de hand van het logo of je op de juiste pagina bent beland
        new DriverUtils().WachtOpElement(driver, logoTT);
        logoTT.isDisplayed();
    }

}
