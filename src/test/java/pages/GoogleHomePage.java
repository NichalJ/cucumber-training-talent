package pages;

import initialization.DriverUtils;
import initialization.EnvironmentData;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleHomePage {

    WebDriver driver;

    public GoogleHomePage(WebDriver driver) {
        // Deze constructor roept een lokale driver op, evenals de variabelen van de scherm elementen
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definitie website elementen
    //==============================================================================

    @FindBy(xpath = "//div[@class='QS5gu sy4vM'][contains(text(),'Alles accepteren')]")
    private WebElement popupButtonIkGaAkkoordByText;

    @FindBy(xpath = "//button[@id='L2AGLb']")
    public WebElement popupButtonIkGaAkkoordByID;

    @FindBy(xpath = "//input[@name='q']")
    public WebElement veldZoekterm;

    @FindBy(xpath = "//span[contains(text(),'Qquest | IT-detachering & IT-Traineeship')]")
    private WebElement dropdownResultaatQquest;

    //==============================================================================
    // Definitie methodes die de interactie uitvoeren van de website elementen
    //==============================================================================

    public void openGoogleHomepage() {
        // Navigeert in de browser naar de website
        String Environment = new EnvironmentData().GetEnvironmentURL(EnvironmentData.EnvironmentName.GOOGLE);
        driver.get(Environment);
    }

    public void klikPopupButtonIkGaAkkoord() {
        new DriverUtils().WachtOpElement(driver,popupButtonIkGaAkkoordByText);
        popupButtonIkGaAkkoordByText.click();
    }

    public void vulZoekTermIn(String zoekterm) {
        // Vult de zoekterm in
        new DriverUtils().WachtOpElement(driver, veldZoekterm);
        veldZoekterm.sendKeys(zoekterm);
    }

    public void clickZoekTerm() {
        // Klikt op het zoekterm veld
        new DriverUtils().WachtOpElement(driver, veldZoekterm);
        veldZoekterm.click();
    }

    public void dropdownResultaatQquest()  {
        // Leest foutmelding af van het scherm
        new DriverUtils().WachtOpElement(driver, dropdownResultaatQquest);
        //return dropdownResultaatQquest.getText();
    }

    public void drukOpEnter() {
        // Drukt op Enter
        new DriverUtils().WachtOpElement(driver, veldZoekterm);
        veldZoekterm.sendKeys(Keys.ENTER);
    }

}
