package initialization;

public class EnvironmentData {

    public enum EnvironmentName {
        //** Hier is het lijstje van omgevingen die gebruikt kunnen worden tijdens het testen **//
        TICKETTONITE,GOOGLE,TST
    }

    public String GetEnvironmentURL(EnvironmentName environmentName) {
        switch (environmentName) {
            case GOOGLE:
                return "https://www.google.nl/";
            case TICKETTONITE:
                return "https://tickettonite-sandbox.mxapps.io/";
        }
        return "https://www.google.nl/";
    }



}
