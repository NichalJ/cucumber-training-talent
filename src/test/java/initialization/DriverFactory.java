/*
 * Defines a global variable for the driver and initializes it at the start of each scenario.
 * The driver is automatically destroyed by calling the destroyDriver() method from the @After hook.
 * The @After hook is defined in the hooks package.
 * 
 */
package initialization;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.HashMap;
import java.util.Map;

public class DriverFactory {

    public enum BrowserName {
        //** Hier is het lijstje van Browsers die gebruikt kunnen worden tijdens het testen **//
        DEFAULT,CHROME,FIREFOX,EDGE,IPHONE,HEADLESS
    }

    protected static WebDriver driver = null;

    public WebDriver startDriverWithBrowser(BrowserName browserName) {
        switch (browserName) {
            case DEFAULT:
                //** Kies hier de default browser waarmee je wil testen ** //
                createNewChromeDriverInstance();
                break;
            case CHROME:
                createNewChromeDriverInstance();
                break;
            case FIREFOX:
                createNewFirefoxDriverInstance();
                break;
            case EDGE:
                createNewEdge90DriverInstance();
                break;
            case IPHONE:
                createNewChromeDriverInstanceIphoneEmulation();
                break;
            case HEADLESS:
                createNewChromeDriverInstanceHeadless();
                break;
        }
        return driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void destroyDriver() {
        // -- Stop en sluit de driver
        driver.quit();
        driver = null;
    }

private void createNewChromeDriverInstanceIphoneEmulation() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver111.exe");

        //**    Gebruik het onderstaande stuk om specifieke dimensies te gebruiken
//        Map<String, Object> deviceMetrics = new HashMap<>();
//        deviceMetrics.put("width", 414);
//        deviceMetrics.put("height", 896);
//        deviceMetrics.put("pixelRatio", 2.0);
//        Map<String, Object> mobileEmulation = new HashMap<>();
//        mobileEmulation.put("deviceMetrics", deviceMetrics);
//        mobileEmulation.put("userAgent", "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1");

        //**    Gebruik het onderstaande stuk om eenvoudig een apparaat te simuleren
        Map<String, String> mobileEmulation = new HashMap<>();

        //**    Kies hier 1 van de onderstaande devices
      mobileEmulation.put("deviceName", "iPhone X");
//      mobileEmulation.put("deviceName", "iPhone 6/7/8");
//      mobileEmulation.put("deviceName", "Galaxy S5");
//      mobileEmulation.put("deviceName", "iPad");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        driver = new ChromeDriver(chromeOptions);
        }

    private void createNewChromeDriverInstance() {
        //**    Start de Chrome driver om te gebruiken met Chrome versie 90.0.
//        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver91.0.exe");
//        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver92.0.exe");
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver111.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--start-maximized");
        // -- Als je later wil maximaliseren gebruik je:
        // --         driver.manage().window().maximize();
        driver = new ChromeDriver(chromeOptions);
    }

    private void createNewEdge90DriverInstance() {
        //**   Start de Microsoft Edge driver.
        System.setProperty("webdriver.edge.driver", "drivers/msedgedriver93.exe");
        driver = new EdgeDriver();
        }

    private void createNewFirefoxDriverInstance() {
        //**    Start de FireFox driver.
        System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"testuitvoer/logs.txt");
        driver = new FirefoxDriver();
        }

    private void createNewChromeDriverInstanceHeadless() {
        //**    Start de Chrome driver.
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver111.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        driver = new ChromeDriver(chromeOptions);
    }


}
