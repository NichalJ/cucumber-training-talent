package initialization;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DriverUtils{

    public String GetStringDateToday()  {
        SimpleDateFormat sdfDate1 = new SimpleDateFormat("dd-MM-yyyy");//dd/MM/yyyy
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        return sdfDate1.format(today);
    }

    public WebElement WachtOpElement(WebDriver driver, WebElement element) {
        // - Zorgt ervoor dat de geautomatiseerde test netjes wacht bij test stappen die wat meer nadenktijd nodig hebben
        // - Definieer hieronder de bedenktijd en aantal keren dat gewacht wordt
        int waitTime = 1;
        int maxAttempts = 5;

        WebDriverWait wait = new WebDriverWait(driver, waitTime);
        int attempts = 0;
        // - Definieer hieronder hoe vaak er gewacht moet worden
        while(attempts < maxAttempts) {
            try {
                wait.until(ExpectedConditions.visibilityOf(element));
                wait.until(ExpectedConditions.elementToBeClickable(element));
                break;
            }  catch(WebDriverException e) {
                // - indien nodig kunnen we aanzetten dat we een screenshot maken
                //new DriverFactory().takeScreenshot();
                // - indien nodig kunnen we aanzetten dat de pagina gerefreshed wordt
                //driver.navigate().refresh();
                attempts++;
            }
        }
        if(attempts == maxAttempts){
            throw new TimeoutException("Failed to find " + element + " after " + maxAttempts + " attempts on page " + driver.getCurrentUrl());
        }
        return element;
    }
    public WebElement WachtOpURL(WebDriver driver, String ExpectedURL) {
        // - Zorgt ervoor dat de geautomatiseerde test netjes wacht bij test stappen die wat meer nadenktijd nodig hebben
        // - Definieer hieronder de bedenktijd en aantal keren dat gewacht wordt
        int waitTime = 1;
        int maxAttempts = 5;

        WebDriverWait wait = new WebDriverWait(driver, waitTime);
        int attempts = 0;
        // - Definieer hieronder hoe vaak er gewacht moet worden
        while(attempts < maxAttempts)
        {
            try {
                wait.until(ExpectedConditions.urlToBe(ExpectedURL));
                break;
            }  catch(WebDriverException e) {
                attempts++;
            }
        }
        if(attempts == maxAttempts){
            throw new TimeoutException("URL not found after " + maxAttempts + " attempts:" +
                    "\n Test waited for  " + ExpectedURL +
                    "\n Test is stuck on " + driver.getCurrentUrl() );
        }
        return null;
    }

    public void takeScreenshot() throws Exception {

        Object driver = new initialization.DriverFactory().getDriver() ;
        File scrn = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        // extracting date for folder name.
        SimpleDateFormat sdfDate1 = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        Date now1 = new Date();
        String strDate1 = sdfDate1.format(now1);

        // extracting date and time for snapshot file
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);

        String filefolder = "./testuitvoer/Screenshots/" + strDate1 + "/";  // create a folder as snap in  your project directory

        // Creating folders and files
        File f = new File(filefolder + strDate + ".jpeg");

        FileUtils.copyFile(scrn, new File(f.getPath()));

    }

}

