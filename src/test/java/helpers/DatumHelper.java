package helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatumHelper {


    public String GetStringDateTomorrow()  {
        SimpleDateFormat sdfDate1 = new SimpleDateFormat("dd-MM-yyyy");//dd/MM/yyyy
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date Tomorrow = calendar.getTime();
        return sdfDate1.format(Tomorrow);
    }
    public String GetStringDateDayAfterTomorrow()  {
        SimpleDateFormat sdfDate1 = new SimpleDateFormat("dd-MM-yyyy");//dd/MM/yyyy
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date DayAfterTomorrow = calendar.getTime();
        return sdfDate1.format(DayAfterTomorrow);
    }

    public String GetStringTimeNow()  {
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        int zoneOffset = calendar.getTimeZone().getRawOffset();
        calendar.add(Calendar.MILLISECOND, -zoneOffset);
        Date now = calendar.getTime();
        return sdfTime.format(now);
    }
    public String GetStringTimeHourAgo()  {
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        int zoneOffset = calendar.getTimeZone().getRawOffset();
        calendar.add(Calendar.MILLISECOND, -zoneOffset);
        calendar.add(Calendar.MINUTE, -61);
        Date hourAgo = calendar.getTime();
        return sdfTime.format(hourAgo);
    }
    public String GetStringTimeOneAndaHalfHoursAgo()  {
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        int zoneOffset = calendar.getTimeZone().getRawOffset();
        calendar.add(Calendar.MILLISECOND, -zoneOffset);
        calendar.add(Calendar.MINUTE, -91);
        Date oneAndaHalfHoursAgo = calendar.getTime();
        return sdfTime.format(oneAndaHalfHoursAgo);
    }
    public String GetStringTime2HoursAgo()  {
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        int zoneOffset = calendar.getTimeZone().getRawOffset();
        calendar.add(Calendar.MILLISECOND, -zoneOffset);
        calendar.add(Calendar.MINUTE, -121);
        Date twohoursAgo = calendar.getTime();
        return sdfTime.format(twohoursAgo);
    }

    public int GetIntCurrentWeekNumber()  {
        SimpleDateFormat week = new SimpleDateFormat("w");
        Calendar calendar = Calendar.getInstance();
        return Integer.parseInt(week.format(calendar.getTime()));
    }

}

