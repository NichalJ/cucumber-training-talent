package steps;

import pages.*;
import initialization.DriverFactory;
import io.cucumber.java.nl.*;

import static org.junit.Assert.*;

public class AllSteps extends DriverFactory {

    // Feature: 0_VoorbeeldGoogle \\

    @Als("Ik ben op de Google homepage")
    public void ikBenOpDeGoogleHomepage() {
        //** Start de driver **//
        new DriverFactory().startDriverWithBrowser(BrowserName.DEFAULT);
        //** Maximaliseer de browser **//
        driver.manage().window().maximize();
        //** Ga naar de pagina **//
        new GoogleHomePage(driver).openGoogleHomepage();
        //** Controleer of de pagina geladen is **//
        assertEquals("https://www.google.nl/", driver.getCurrentUrl());
        //** Klik op de popup om cookies te aanvaarden **//
        new GoogleHomePage(driver).klikPopupButtonIkGaAkkoord();
    }

    @Wanneer("Ik invul {string}")
    public void ikInvul(String zoekterm) {
        //** Roep een methode aan die een zoekterm invult **//
        new GoogleHomePage(driver).vulZoekTermIn(zoekterm);
        new GoogleHomePage(driver).clickZoekTerm();
    }

    @Dan("Zie ik in een dropdown Qquest als resultaat")
    public void zieIkInEenDropdownQquestAlsResultaat() {
        //** Roep een methode aan die een controle doet **//
        new GoogleHomePage(driver).dropdownResultaatQquest();
    }

    @En("Ik druk op Enter")
    public void ikDrukOpEnter() {
        //** Roep een methode aan die op enter drukt **//
        new GoogleHomePage(driver).drukOpEnter();
    }

    @Dan("Zie ik dat het eerste zoekresultaat Welkom bij Qquest is")
    public void zieIkDatHetEersteZoekresultaatWelkomBijQquestIs() {
        //** Roep een methode aan die een controle doet **//
        new GoogleResultPage(driver).zoekResultaatQquest();
    }

    // Feature: 1_TeamSelecteren \\

    @Als("Ik ben op de team selectie pagina")
    public void ikBenOpDeTeamSelectiePagina() {
        //** Start de driver **//
        new DriverFactory().startDriverWithBrowser(BrowserName.DEFAULT);
        //** Maximaliseer de browser **//
        driver.manage().window().maximize();
        //** Ga naar de pagina **//
        new TeamSelecterenPage(driver).openTicketTonite();
        //** Controleer of de pagina geladen is **//
        assertEquals("https://tickettonite-sandbox.mxapps.io/", driver.getCurrentUrl());
    }

    @Wanneer("Ik klik op het team Fantestic Automatics")
    public void ikKlikOpHetTeamFantesticAutomatics() {
        new TeamSelecterenPage(driver).clickTeamNaam();
    }

    @Dan("Ik zie de beginpagina van de Ticket Tonite app")
    public void ikZieDeBeginpaginaVanDeTicketToniteApp() {
        new TeamSelecterenPage(driver).checkLogoTT();
    }

    // Feature: 2_Registreren \\

    @En("Ik klik op de knop Registreren")
    public void ikKlikOpDeKnopRegistreren() {

    }

    @Dan("Ik zie dat ik mezelf gratis kan registreren")
    public void ikZieDatIkMezelfGratisKanRegistreren() {

    }

    @Als("Mijn e-mailadres is {string}")
    public void mijnEMailadresIsEMail(String emailadres) {
        new RegistreerPage(driver).inputEmail(emailadres);
    }

    @En("Mijn gebruikersnaam is {string}")
    public void mijnGebruikersnaamIsGebruikersnaam() {

    }

    @En("Mijn voornaam is {string}")
    public void mijnVoornaamIsVoornaam() {

    }

    @En("Mijn achternaam is {string}")
    public void mijnAchternaamIsAchternaam() {

    }

    @En("Mijn wachtwoord is {string}")
    public void mijnWachtwoordIsWachtwoord() {

    }

    @En("Mijn eerste wachtwoord is ook {string}")
    public void mijnEersteWachtwoordIsOokWachtwoord() {

    }

    @En("Ik klik op Registreren")
    public void ikKlikOpRegistreren() {
        new RegistreerPage(driver).clickRegistreren();
    }

    @Dan("Mijn account is aangemaakt")
    public void mijnAccountIsAangemaakt() {
        new RegistreerPage(driver).checkLogin();
    }

    // Feature: 3_Inloggen \\

    @Dan("Ik zie de inlogpagina")
    public void ikZieDeInlogpagina() {
        new RegistreerPage(driver).checkLogin();
    }

    @En("Mijn inlognaam is {string}")
    public void mijnInlognaamIsGebruikersnaam( {
    }


    @Dan("Ik ben ingelogd in Ticket Tonite")
    public void ikBenIngelogdInTicketTonite() {
        new InlogPage(driver).checkMijnEvents();
    }

    @Dan("Ik druk op de groene knop Confetti en schreeuw keihard AAAAAAAAAAAA")
    public void ikDrukOpDeGroeneKnopConfettiEnSchreeuwKeihardAAAAAAAAAAAA() {

    }
}

