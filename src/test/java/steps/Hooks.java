package steps;

import initialization.DriverFactory;
import initialization.DriverUtils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.WebDriver;

public class Hooks {
    WebDriver driver;

    @Before
    public void before(Scenario scenario) {
        //  - Standaard stappen voor het uitvoeren van een test
//        System.out.println("De volgende test is: "+scenario.getName());
    }

    @After
    public void afterScenario(Scenario scenario) throws Exception {
        //  - Standaard stappen na het uitvoeren van een test

        //  - Haal de driver op en kijk of deze driver bestaat
        driver = new DriverFactory().getDriver();
        if (driver != null) {
            //  - Maak een screenshot
            if (scenario.isFailed()) {
                new DriverUtils().takeScreenshot();
                System.out.println("Fout gevonden in " + scenario.getName());
                System.out.println("Screenshot gemaakt op pagina " + driver.getCurrentUrl());
            }
            // - Als je altijd een screenshot na afloop wil kan dat hier aangezet worden
            //new DriverUtils().takeScreenshot();

        // - Sluit de driver af
        new DriverFactory().destroyDriver();
        }
    }
}