# Selenium Cucumber Training 2023

## Hoi toppers!

Dit is de allereerste versie van de Selenium Cucumber Training die je krijgt tijdens de training testautomatisering.

Het is nog in ontwikkeling, dus al jullie feedback en tips zijn van harte welkom! Have fun :) 

## Benodigdheden

+ IntelliJ Community Edition -> https://www.jetbrains.com/idea/download/
+ Java 17 of 18
+ Google Chrome
+ ChroPath -> https://chrome.google.com/webstore/detail/chropath/ljngjbnaijcbncmcnjfhigebomdlkcjo

## Importeren

Hoe importeer je het project naar IntelliJ?
- Klik op Clone
- Klik op het icoontje onder 'Clone with HTTPS'
- In IntelliJ ga je naar File -> New -> Project from Version Control
- Plak de URL in het venster
- Klik op Clone
- Het feestje kan beginnen!

## Good to know

- Het kan zijn dat je Maven moet importeren
- Het kan ook zijn dat je de juiste versie van Java moet selecteren
- Het kan heel goed zijn dat je geen flauw idee hebt wat je moet doen

## Opdrachten

**Oefenen met Google**

1. Open de feature file _0_VoorbeeldGoogle_ en check of de scenario's werken.
2. Wat gebeurt er bij de stap 'Ik ben op de Google homepage'? _(hint: Ctrl + linker muisknop)_
3. Zoek met ChroPath de Xpath van het eerste Qquest zoekresultaat en check of deze hetzelfde is als de Xpath in de page file.

**TicketTonite**

In de eerste test trainingen hebben jullie al gewerkt met de TicketTonite app, met liefde gebouwd door onze zeer gewaardeerde Mendix collega's. We gaan deze app stapje voor stapje voorzien van geautomatiseerde tests. We hebben de team selectie pagina, de registreer pagina en de inlogpagina al volledig geautomatiseerd. 

Echter is er één probleempje, namelijk dat er stukken code zijn verdwenen :-(

Zowel in de feature files, de step file als in de page files zijn er hele stukken code volledig van de aardbodem verdwenen. Helpen jullie ons mee om de testautomatisering weer werkend te krijgen?

_Opdracht 1 - Team Selecteren_

1. Controleer of het scenario werkt.
2. Bekijk de step file om te zien welke onderdelen de stappen bestaan.
3. Bekijk de page file om te zien welke Xpath er gebruikt wordt en hoe de methodes worden toegepast.

_Opdracht 2 - Registreren_

1. Controleer of het scenario werkt.
2. Open de step en page file die horen bij deze feature file.
3. Repareer de 'kapotte' steps in de step file en kijk daarbij ook naar de methodes en xpath's in de page file die nog ontbreken.

_Opdracht 3 - Inloggen_

1. Repareer de scenario's. _(tip: check bij de eerdere opdrachten hoe het daar is gegaan)_
2. Open de step en page file die horen bij deze feature file.
3. Repareer de 'kapotte' steps in de step file en kijk daarbij ook naar de methodes en xpath's in de page file die nog ontbreken.
4. Werkt alles weer? Trap het laatste scenario af en schreeuw keihard AAAAAAAAAA zodra je confetti ziet!

## Boodschap

Deze readme wordt vast nog wel een keer aangevuld, groetjes